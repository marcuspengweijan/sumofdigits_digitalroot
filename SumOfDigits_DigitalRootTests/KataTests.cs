﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SumOfDigits_DigitalRoot.Tests
{
    [TestClass()]
    public class KataTests
    {
        [DataTestMethod]
        [DataRow(16, 7, DisplayName = "16 should return 7")]
        [DataRow(456, 6, DisplayName = "456 should return 6")]
        [DataRow(0, 0, DisplayName = "0 should return 0")]
        public void DigitalRootTest(long input, int expected)
        {
            Assert.AreEqual(expected, Kata.DigitalRoot(input));
        }
    }
}