﻿using System;
using System.Linq;

namespace SumOfDigits_DigitalRoot
{
    public class Kata
    {
        public static int DigitalRoot(long n)
        {
            return n < 10 
                ? Convert.ToInt32(n) 
                : DigitalRoot(n.ToString().Select(x => int.Parse(x.ToString())).Aggregate((x, y) => x + y));
        }
    }
}
